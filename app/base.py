import utils, random, os
import Queue, threading

def safe_dec_enc(data,basic=False):
    if data:
        if isinstance(data, unicode):
            if basic:
                return data.encode('ascii','ignore')
            return data.encode('utf-8','ignore')
        else:
            if basic:
                # REMOVE NON-STANDARD UNICODE-POINTS FROM BYTE-STRING
                return data.decode('ascii','ignore').encode('ascii')
            return data.decode('utf-8','ignore').encode('utf-8')
    return ''

class Atomic_Counter:
    def __init__(self, rows, ROI, initial):
        self.rows = rows
        self.value = initial
        self.ROI = ROI
        self._lock = threading.Lock()

    def update(self, key, status):
        with self._lock:
            if self.value != 0 and key in self.ROI:
                self.value -= 1
                self.ROI.pop(key)
                self.rows[key]['status'] = status
                return True

    def getItem(self):
        with self._lock:
            if self.value != 0:
                key = random.choice(self.ROI.keys())
                data = self.ROI[key]
                return key, data, self.value
            return None

class Base:
    def __init__(self, name):
        self.AC = None
        self.name = name
        self.fields, self.rows = ['status'], []
        self.main_fields = ['Name', 'Locality', 'Street Address', 'VILLAGE_NAME']
        self.initLoad()
        self.WRITTEN, self.w_lock = False, threading.Lock()

    def form_element(self, row):
        data = {
            'ORIG': {
                'Name':row['Name'],
                'Locality':row['Locality'],
                'Street Address':row['Street Address'],
                'City':row['City'],
                'Village':row['VILLAGE_NAME'],
                'Pincode':row['Pincode']
            },
            'PRED': {
                'Name':row['h_name'],
                'Address':row['h_address'],
                'MATCH':row['h_word']
            }
        }
        for key1 in data.keys():
            for key2 in data[key1]:
                data[key1][key2] = safe_dec_enc(data[key1][key2], True)
        return data

    def initLoad(self):
        # CACHE LOAD IF PRESENT, DUE TO WRITES AFTER EVERY 50 SUBMISSIONS (to recover from critical app falures with least loss)
        out = os.path.join(os.path.dirname(os.path.dirname(self.name)), 'output', os.path.basename(self.name))
        if os.path.exists(out):
            self.fields = []
            utils.read_csv(out, self.fields, self.rows)
            print 'CACHE LOADED'
        else:
            utils.read_csv(self.name, self.fields, self.rows)

        #self.rows = self.rows[:200]
        ROI, count = dict(), 0
        for idx, row in enumerate(self.rows):
            if row['FATE'] == 'OPERATIONS_TEAM' and row['status'] not in ['0', '1']:
                data = self.form_element(row)
                ROI[idx] = data
                count += 1
        self.AC = Atomic_Counter(self.rows, ROI, count)

    def get(self):
        it = self.AC.getItem()
        if it is None:
            if not self.WRITTEN:
                self.writeFile()
                self.WRITTEN = True
            return None, None, None
        else:
            return it

    def writeFile(self):
        with self.w_lock:
            out = os.path.join(os.path.dirname(os.path.dirname(self.name)), 'output', os.path.basename(self.name))
            utils.write_csv(out, self.fields, self.rows)

    def update(self, key, status):
        stat = self.AC.update(int(key), status)
        # Save after every multiple of 50 standing ROI counts
        if stat and self.AC.value%50 == 0:
            self.writeFile()
