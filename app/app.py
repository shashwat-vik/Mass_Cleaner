from flask import Flask, render_template, request, redirect, url_for
from base import Base

app = Flask(__name__)
base_obj = Base('/home/ubuntu/shashwat/Mass_Cleaner/input/tx___Gujarat.csv')

@app.route("/play")
def play():
    key, data, count = base_obj.get()
    if key is None:
        return render_template('over.html')
    return render_template('play.html', key=key, data=data, count=count)

@app.route("/update", methods=['POST'])
def update():
    key, status = request.form['key'].encode('ascii'), request.form['status'].encode('ascii')
    base_obj.update(key, status)
    return redirect(url_for('play'))

@app.route("/fdc29e5eaf0d8b70e0d23fd177ab208a")
def emergencySave():
    base_obj.writeFile()
    return "SAVED"

if __name__ == '__main__':
    app.run('0.0.0.0', debug=True, port=5000, threaded=True)
