import csv, os

def read_csv(file_path, fields, rows):
    with open(file_path, 'r') as csvFile:
        reader = csv.DictReader(csvFile, dialect=csv.excel)
        if fields:
            reader.fieldnames.extend(fields)
            del fields[:]
        fields.extend(reader.fieldnames)
        rows.extend(reader)
    print '## READ FILE:', os.path.basename(file_path)

def write_csv(file_path, fields, rows):
    with open(file_path, 'w') as csvFile:
        writer = csv.DictWriter(csvFile, fieldnames=fields, lineterminator='\n')
        writer.writerow(dict(zip(fields,fields)))
        for row in rows:
            writer.writerow(row)
    print '## FILE WRITTEN'
